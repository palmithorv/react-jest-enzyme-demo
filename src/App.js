import React, { Component } from "react";
import logo from "./logo.svg";
import axios from "axios";
import "./App.css";
import {
  Grid,
  Row,
  Button,
  FormGroup,
  FormControl,
  Alert
} from "react-bootstrap";
import Show from "./show";

export const SHOWS_BASE_URL = "http://api.tvmaze.com/shows";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      show: undefined,
      showIdToSearchFor: "",
      error: undefined
    };
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="container">
          <Grid>
            <Row>
              <form>
                <FormGroup>
                  <FormControl
                    type="text"
                    label="Text"
                    placeholder="Enter show id"
                    value={this.state.value}
                    onChange={this.handleInput}
                  />
                </FormGroup>
                <Button
                  id="searchShowButton"
                  disabled={this.state.isLoading}
                  onClick={!this.state.isLoading ? this.fetchShow : null}
                  bsStyle="primary"
                >
                  Search
                </Button>
              </form>
            </Row>
            {this.state.show && (
              <Row>
                <Show show={this.state.show} />
              </Row>
            )}
            {this.state.error && (
              <Row>
                <Alert bsStyle="danger">
                  <strong>Holy guacamole!</strong> {this.state.error}
                </Alert>
              </Row>
            )}
          </Grid>
        </div>
      </div>
    );
  }

  handleInput = input => {
    this.setState({ showIdToSearchFor: input.target.value });
  };

  fetchShow = async () => {
    this.setState({ isLoading: true });
    try {
      const response = await axios.get(
        `${SHOWS_BASE_URL}/${this.state.showIdToSearchFor}`
      );
      this.setState({
        show: response.data,
        isLoading: false,
        error: undefined
      });
    } catch (err) {
      this.setState({
        isLoading: false,
        show: undefined,
        error: "Error fetching show"
      });
    }
  };
}

export default App;
