import React from "react";


export default class Show extends React.Component {

    render() {
        console.log(this.props);
      return (
        <div>
          <h1>{this.props.show.name}</h1>
          <div dangerouslySetInnerHTML={{__html: this.props.show.summary}}>
          </div>
        </div>
      );
    }
  }