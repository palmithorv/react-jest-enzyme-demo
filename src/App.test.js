import React from "react";
import Adapter from "enzyme-adapter-react-16";
import Enzyme, { shallow } from "enzyme";
import { waitForState } from "enzyme-async-helpers";
import App from "./App";
import Show from "./show";

import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { SHOWS_BASE_URL } from "./App";

Enzyme.configure({ adapter: new Adapter() });

const mock = new MockAdapter(axios);

it("renders without crashing", () => {
  const wrapper = shallow(<App />);
  expect(wrapper.find("h1").text()).toBe("Welcome to React");
});

it("should fetch a tv show, store it in the state and show", async () => {
  const id = 5;
  // Mock a single GET request to tv maze shows
  mock.onGet(`${SHOWS_BASE_URL}/${id}`).replyOnce(200, {
    id,
    name: "True Detective",
    summary:
      "<p>The series stars Matthew McConaughey and Woody Harrelson as Louisiana detectives Rust Cohle and Martin Hart, whose lives collide and entwine during a 17-year hunt for a killer, ranging from the original investigation of a bizarre murder in 1995 to the reopening of the case in 2012. Michelle Monaghan also stars as Hart's wife, Maggie, who struggles to keep her family together as the men in her life become locked in a cycle of violence and obsession.</p>"
  });

  const wrapper = shallow(<App />);
  expect(wrapper.find(Show).length).toBe(0);
  
  // todo update so that this uses the input field
  wrapper.state().showIdToSearchFor = "5"; 
  wrapper.find("#searchShowButton").simulate("click");
  await waitForState(wrapper, state => state.isLoading === false);
  expect(wrapper.update().state().show.id).toBe(id);
  expect(wrapper.find(Show).length).toBe(1);
});

it("should fail getting a tv show", async () => {
  // verify that an error is shown
});

it("should disable the search button fetching tv show", async () => {
  
});

